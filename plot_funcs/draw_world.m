function [] = draw_world(x, np, nl, nu)

%% state variables
xp = reshape(x(1:np*5), 5, np)';
xl = reshape(x(np*5+1:np*5+nl*2), 2, nl)';
xu = reshape(x(np*5+nl*2+1:np*5+nl*2+nu*2), 2, nu)';

lmx = xl(:,1);
lmy = xl(:,2);

x = xp(:,1);
y = xp(:,2);
theta = xp(:,3);

T = xu(:,1);
w = xu(:,2);

figure(2); clf(2); hold on;

%% draw robot

plot(x, y, '.-', 'MarkerSize', 5, 'LineWidth', 1, 'Color', [0 0 1]);

radius = 1.5;
th = 0:pi/50:2*pi;
for i = 1:np
    xunit = radius * cos(th) + x(i);
    yunit = radius * sin(th) + y(i);
    plot(xunit, yunit, 'LineWidth', 1, 'Color', [1 0 0]);
    
    ui = radius * cos(theta(i));
    vi = radius * sin(theta(i));
    quiver(x(i), y(i), ui, vi, 'LineWidth', 1, 'Color', [1 0 0]);
end

%% draw world

% draw landmarks 
plot(lmx, lmy, 'r*');

% draw measurements
for i = 1:n_poses
    for j = 1:n_landmarks
        if lms(i,j)
            plot([x(i);lmx(j)],[y(i);lmy(j)],'g--');
        end
    end
end

% subplot(4,4,[1,2,5,6,9,10,13,14]);
% ylim([0 75]); xlim([0 100]);
% hold off;

% % Draw observation
% for i = 1:n_poses
%     for j = 1:n_landmarks
%         if lms(i,j)
%             plot([x(i);lmx(j)],[y(i);lmy(j)],'g--');
%         end
%     end
% end

% Draw tracks
plot(x,y,'b.-','MarkerSize',20);
% Draw heading and force
% m = [x' y' [x+T.*cos(theta)]' [y+T.*sin(theta);]'];
% plot([m(:,1) m(:,3)]',[m(:,2) m(:,4)]','b-','LineWidth',2);
% grid on;

end