function [] = draw_world_with_gt(x, x_gt, np, nl, nu, lm_obs, robot_radius)

%% state variables

xp = reshape(x(1:np*5), 5, np)';
xl = reshape(x(np*5+1:np*5+nl*2), 2, nl)';
xu = reshape(x(np*5+nl*2+1:np*5+nl*2+nu*2), 2, nu)';

lmx = xl(:,1);
lmy = xl(:,2);
x = xp(:,1);
y = xp(:,2);
theta = xp(:,3);
T = xu(:,1);
w = xu(:,2);

xp_gt = reshape(x_gt(1:np*5), 5, np)';
xl_gt = reshape(x_gt(np*5+1:np*5+nl*2), 2, nl)';
xu_gt = reshape(x_gt(np*5+nl*2+1:np*5+nl*2+nu*2), 2, nu)';

lmx_gt = xl_gt(:,1);
lmy_gt = xl_gt(:,2);
x_gt = xp_gt(:,1);
y_gt = xp_gt(:,2);
theta_gt = xp_gt(:,3);
T_gt = xu_gt(:,1);
w_gt = xu_gt(:,2);

figure(2); clf(2); hold on;
set(gcf, 'Position', get(0, 'Screensize'));
% subplot(4,4,[1,2,5,6,9,10,13,14]);
% ylim([0 100]); xlim([0 100]);

%% draw robot ground truth

plot(x_gt, y_gt, '.-', 'MarkerSize', 10, 'LineWidth', 1, 'Color', [0 0.4470 0.7410]);
radius = robot_radius;
th = 0:pi/50:2*pi;
for i = 1:np
    xunit = radius * cos(th) + x_gt(i);
    yunit = radius * sin(th) + y_gt(i);
    plot(xunit, yunit, 'LineWidth', 1, 'Color', [0 0.4470 0.7410]);

    tri_pts = [x_gt(i) + radius * cos(theta_gt(i)), y_gt(i) + radius * sin(theta_gt(i)); ...
           x_gt(i) - radius * sin(theta_gt(i)), y_gt(i) + radius * cos(theta_gt(i)); ...
           x_gt(i) + radius * sin(theta_gt(i)), y_gt(i) - radius * cos(theta_gt(i))];
       
    patch([tri_pts(2,1) tri_pts(1,1); tri_pts(1,1) tri_pts(3,1); tri_pts(3,1) tri_pts(2,1)], ...
        [tri_pts(2,2) tri_pts(1,2); tri_pts(1,2) tri_pts(3,2); tri_pts(3,2) tri_pts(2,2)], [0 0.4470 0.7410]);
end

%% draw robot

plot(x, y, '.-', 'MarkerSize', 10, 'LineWidth', 1, 'Color', [0.8500 0.3250 0.0980]);
radius = robot_radius;
th = 0:pi/50:2*pi;
for i = 1:np
    xunit = radius * cos(th) + x(i);
    yunit = radius * sin(th) + y(i);
    plot(xunit, yunit, 'LineWidth', 1, 'Color', [0.8500 0.3250 0.0980]);

    tri_pts = [x(i) + radius * cos(theta(i)), y(i) + radius * sin(theta(i)); ...
           x(i) - radius * sin(theta(i)), y(i) + radius * cos(theta(i)); ...
           x(i) + radius * sin(theta(i)), y(i) - radius * cos(theta(i))];

    patch([tri_pts(2,1) tri_pts(1,1); tri_pts(1,1) tri_pts(3,1); tri_pts(3,1) tri_pts(2,1)], ...
        [tri_pts(2,2) tri_pts(1,2); tri_pts(1,2) tri_pts(3,2); tri_pts(3,2) tri_pts(2,2)], [0.8500 0.3250 0.0980]);
end

%% draw world

lms = (lm_obs==1);

% draw landmarks ground truth
plot(lmx_gt, lmy_gt, '*', 'MarkerSize', 10, 'LineWidth', 1.5, 'Color', [0 0.4470 0.7410]);

% draw landmarks 
plot(lmx, lmy, '*', 'MarkerSize', 10, 'LineWidth', 1, 'Color', [0.8500 0.3250 0.0980]);

% draw measurements
% for i = 1:np
%     for j = 1:nl
%         if lms(i,j)
%             plot([x(i);lmx(j)],[y(i);lmy(j)],'g--');
%         end
%     end
% end

% subplot(4,4,[1,2,5,6,9,10,13,14]);
% ylim([0 75]); xlim([0 100]);
% hold off;
% Draw tracks
% plot(x,y,'b.-','MarkerSize',20);
% Draw heading and force
% m = [x' y' [x+T.*cos(theta)]' [y+T.*sin(theta);]'];
% plot([m(:,1) m(:,3)]',[m(:,2) m(:,4)]','b-','LineWidth',2);
% grid on;

end