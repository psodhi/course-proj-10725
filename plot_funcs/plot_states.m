function [] = plot_states(x, np, nl, nu)

xp = reshape(x(1:np*5), 5, np)';
xl = reshape(x(np*5+1:np*5+nl*2), 2, nl)';
xu = reshape(x(np*5+nl*2+1:np*5+nl*2+nu*2), 2, nu)';

lmx = xl(:,1);
lmy = xl(:,2);

subplot(4,4,[1,2,5,6,9,10,13,14]);
%ylim([0 75]); xlim([0 100]);
% hold off;
hold on;

% % Draw observation
% for i = 1:n_poses
%     for j = 1:n_landmarks
%         if lms(i,j)
%             plot([x(i);lmx(j)],[y(i);lmy(j)],'g--');
%         end
%     end
% end

% Draw landmarks 
plot(lmx, lmy, 'r*');

% Draw tracks
plot(xp(:,1),xp(:,2),'b.-','MarkerSize',20);
% Draw heading and force
% m = [x' y' [x+T.*cos(theta)]' [y+T.*sin(theta);]'];
% plot([m(:,1) m(:,3)]',[m(:,2) m(:,4)]','b-','LineWidth',2);
grid on;

end