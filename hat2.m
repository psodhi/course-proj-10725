function [ so2 ] = hat2( theta )
so2 = [0 -theta;
       theta 0];
end

