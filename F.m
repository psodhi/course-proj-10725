function [dxdt,dydt,drdt,dvxdt,dvydt ] = F( x,y,theta,vx,vy,T,w,m )
% dxdt = vx
% dydt = vy
% drdt = (w_R_b) dt*w
% dvxdt = T/m*R*[1;0]
% dvydt = 0

dxdt = vx;
dydt = vy;
drdt = w;
dvdt = T/m*exp_map2(hat2(theta))*[1;0];
dvxdt = dvdt(1);
dvydt = dvdt(2);

end

