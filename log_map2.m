function [ so2] = log_map2( SO2 )
    R=SO2;
    theta = atan2(R(2,1),R(1,1));
    so2 = [0 -theta;
           theta -0];
end