clear;
clc;
close all;

%% Front End : Robot Dynamics & World Simulator

% Run robot simulator and obtain sequence of
% np poses, nl landmark locations and nu control inputs

% n_poses, n_landmark, mass, dt
dt = 1;
mass = 0.2;
[x,y,psi,vx,vy,T,w,lm_obs,lm_locs] = generate_world(5,20,mass,dt);

% Remove unobserved landmarks
lm_locs = lm_locs((any(lm_obs,1))', :);
lm_obs = lm_obs(:, any(lm_obs,1));

xp_k_gt = [x',y',psi',vx',vy'];
xl_k_gt = lm_locs;
xu_k_gt = [T',w'];

% Returns current time tc state estimates {xp_k, xl_k, xu_k}
% Returns measurements up till current time tc {z, pair_idxs}

%% Front End : Measurements Generation

% Generate set of (range, bearing) observations with/without noise
sigma_r = 0;  % 0.2m 
sigma_t = 0.0; % 0.1 radian
[z, pair_idxs] = generate_observations(xp_k_gt, xl_k_gt, lm_obs, sigma_r, sigma_t);

%% Back End : SQP Optimization

% OPTIMIZE sequence of np poses, nl landmark locations and nu control inputs
% np : no. of poses up till current time tc
% nl : no. of landmark locations (assumed all landmarks are visible)
% nu : no. of control inputs up till current time tc
% PRIMAL STATE VARIABLES
% xp_k : np x 5 [x y psi vx vy]
% xl_k : nl x 2 [lx ly]
% xu_k : nu x 2 [T w]
% MEASUREMENTS
% z: m x 2 -- (range,   bearing) values
% pair_idxs : m x 2 -- (pose idx, landmark idx) for each z
% DUAL STATE VARIABLES
% lambda_k : (neq*5) x 1

np = size(xp_k_gt,1);
nl = size(xl_k_gt,1);
nu = size(xu_k_gt,1);
neq = np-1;

% Initialize state variables for optimization
xp_k = xp_k_gt.*(0.8+0.2*rand(np,5));
xl_k = xl_k_gt.*(0.8+0.2*rand(nl,2));
xu_k = xu_k_gt;
xp_k_trans = xp_k'; xl_k_trans = xl_k'; xu_k_trans = xu_k';
x_k = [xp_k_trans(:) ; xl_k_trans(:); xu_k_trans(:)]; % primal variables
lambda_k = rand(neq*5, 1); % dual variables

xp_k_gt_trans = xp_k_gt'; xl_k_gt_trans = xl_k_gt'; xu_k_gt_trans = xu_k_gt';
x_k_gt = [xp_k_gt_trans(:) ; xl_k_gt_trans(:); xu_k_gt_trans(:)];

num_sqp_iters = 20;
delta_x = zeros(length(x_k),1);
x_k_alliters = zeros(size(x_k,1), num_sqp_iters);


for k = 1:num_sqp_iters
    fprintf('Running SQP Iteration : %d\n', k);

    x_k_alliters(:,k) = x_k;
    [x_kplus1, lambda_kplus1, delta_x] = optimize_sqp(x_k, delta_x, np, nl, nu, lambda_k, z, pair_idxs, dt, mass);
    
    draw_world_with_gt(x_k, x_k_gt, np, nl, nu, lm_obs, 1.0);
    drawnow;
    subplot(3,4,[3,4])
    plot(x_k(3), 'o'); hold on;

    x_k = x_kplus1;
    lambda_k = lambda_kplus1;
end

%% Plotting

% Error Plots
% plot_state_errors(x_k, x_k_gt);

% Draw Initial and Final States




