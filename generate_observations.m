function [z, pair_idxs] = generate_observations(xp_k_gt, xl_k_gt, lm_obs, sigma_r, sigma_t)
    sparse_lm_obs = sparse(lm_obs);
    [pose_idx,lm_idx,~] = find(sparse_lm_obs);
    for k=1:length(pose_idx)
       lm_local = exp_map2(hat2(xp_k_gt(pose_idx(k),3)))'*(xl_k_gt(lm_idx(k),:) - xp_k_gt(pose_idx(k),1:2))';
       z(k,1) = sqrt(lm_local(1)^2 + lm_local(2)^2) + sigma_r * (rand(1,1)-0.5);
       z(k,2) = atan2(lm_local(2), lm_local(1)) + sigma_t * (rand(1,1)-0.5);
    end
    pair_idxs = [pose_idx, lm_idx];
    
    subplot(4,4,[1,2,5,6,9,10,13,14]);
    hold on;
    % Draw observation
%     for i = 1:length(z)
%        p1 = plot([xp_k_gt(pair_idxs(i,1),1);xp_k_gt(pair_idxs(i,1),1)+z(i,1)*cos(xp_k_gt(pair_idxs(i,1),3)+z(i,2))], ...
%             [xp_k_gt(pair_idxs(i,1),2);xp_k_gt(pair_idxs(i,1),2)+z(i,1)*sin(xp_k_gt(pair_idxs(i,1),3)+z(i,2))],'b--');
%        p1.Color(4) = 0.5;
%     end

end

