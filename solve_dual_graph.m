function [lambda_kplus1] = solve_dual_graph(x_k, np, nl, nu, neq, delta_x, J_obj, H_obj, J_eq, H_eq, dt, mass)

% DUAL FACTOR
A = J_eq';
b = - (H_obj*delta_x + J_obj + H_eq*delta_x);

% QR Factorization
if issparse(A)
    R = qr(A); 
else
    R = triu(qr(A));
end
lambda_kplus1 = R\(R'\(A'*b));

% one step of iterative refinement
% r = b - A*x;
% err = R\(R'\(A'*r));
% x = x + err;

end