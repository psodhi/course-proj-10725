 function [ SO2 ] = exp_map2( so2 )
    theta = so2(2,1);
    SO2 = [cos(theta) -sin(theta); sin(theta) cos(theta)];    
end