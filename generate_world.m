function [x,y,theta,dx,dy,T,w,lm_obs,lm_locs] = generate_world(n_poses, n_landmarks, m, dt)
%% Generate Landmarks
lx = randi([1 100],1, 10000);
ly = randi([1 100],1, 10000);
minAllowableDistance = 10;
% Initialize first point.
lmx = lx(1);
lmy = ly(1);
% Generate random landmarks
counter = 2;
k=2;
while counter <= n_landmarks
	% Get a trial point.
	thisX = lx(k);
	thisY = ly(k);
	% See how far is away from existing keeper points.
	distances = sqrt((thisX-lmx).^2 + (thisY - lmy).^2);
	minDistance = min(distances);
	if minDistance >= minAllowableDistance
		lmx(counter) = thisX;
		lmy(counter) = thisY;
		counter = counter + 1;
    end
    k=k+1;
end

%% Generate Trajectory based on kinematics
% x = x + vx*dt
% y = y + vy*d
% theta = log(exp(r)exp(dt*w))
% vx = vx + cos(r)*a*dt
% vy = vy + sin(r)*a*dt
% dxdt = vx
% dydt = vy
% drdt = (w_R_b) dt*w
% dvxdt = T/m*R*[1;0]
% dvydt = 0
% u = [T w]

xr = randi([0 100],1, 10000);
yr = randi([45 50 ],1, 10000);
Tr = rand(1,n_poses)*5;
wr = (rand(1,n_poses)-0.5)*2;
sensor_range = 50;
lms = zeros(n_poses,n_landmarks);

x(1) = 0;
y(1) = 50;
theta(1) = 0;
dx(1) = 0;
dy(1) = 0;
dvx(1) = 0;
dvy(1) = 0;
T(n_poses) = 0;
w(n_poses) = 0;

% Generate random poses
counter = 2;
k = 2;

% Calculating landmark seen by first pose
for i = 1:n_landmarks
    distance = sqrt((lmx(i)-x(1)).^2 + (lmy(i) - y(1)).^2);
    if distance < sensor_range
        lms(1,i) = 1;
    end
end

while counter <= n_poses
	% Control input random.
    T(k-1) = Tr(k-1);
    w(k-1) = wr(k-1);
    
    dr(k) = w(k-1)+w(k-1);
    theta(k) = vee2(log_map2(exp_map2(hat2(theta(k-1)))*exp_map2(hat2(dt/2*dr(k)))));
    
    dv = T(k-1)/m*exp_map2(hat2(theta(k-1)))*[1;0]; % last accel in world [dvx dvy]
    dv1 = T(k-1)/m*exp_map2(hat2(theta(k)))*[1;0];  % curr accel in world [dvx dvy]
    dvx(k) = dv(1) + dv1(1);
    dvy(k) = dv(2) + dv1(2);
    
    dx(k) = dx(k-1) + dt/2*dvx(k);
    dy(k) = dy(k-1) + dt/2*dvy(k);
    
	x(k) = x(k-1) + dt/2*(dx(k-1)+dx(k));
	y(k) = y(k-1) + dt/2*(dy(k-1)+dy(k));
    
    % Calculating landmark seen
    for i = 1:n_landmarks
        distance = sqrt((lmx(i)-x(k)).^2 + (lmy(i) - y(k)).^2);
        if distance < sensor_range
            lms(k,i) = 1;
        end
    end
    
    % ================= Assertion Check ========================
   [cdx1,cdy1,cdr1,cdvx1,cdvy1 ] = F(x(k),y(k),theta(k),dx(k),dy(k),T(k-1),w(k-1),m);      % F(x(i+1),u(i))
   [cdx,cdy,cdr,cdvx,cdvy ] = F(x(k-1),y(k-1),theta(k-1),dx(k-1),dy(k-1),T(k-1),w(k-1),m); % F(x(i),u(i))
   c_x = x(k-1) + dt/2*(cdx1+cdx);
   c_y = y(k-1) + dt/2*(cdy1+cdy);
   c_r = vee2(log_map2(exp_map2(hat2(theta(k-1)))*exp_map2(hat2(dt/2*(cdr1+cdr)))));
   c_dx = dx(k-1) + dt/2*(cdvx1+cdvx);
   c_dy = dy(k-1) + dt/2*(cdvy1+cdvy);
   assert(c_x == x(k));
   assert(c_y == y(k));
   assert(c_r == theta(k));
   assert(c_dx == dx(k));
   assert(c_dy == dy(k));
   % ===========================================================

   counter = counter + 1;
   k = k+1;
end

lm_obs = lms;
lm_locs = [lmx', lmy'];

%% Plotting
subplot(4,4,[1,2,5,6,9,10,13,14])
hold on;
% Draw observation
for i = 1:n_poses
    for j = 1:n_landmarks
        if lms(i,j)
            plot([x(i);lmx(j)],[y(i);lmy(j)],'g--');
        end
    end
end
% Draw landmarks 
plot(lmx, lmy, 'k*');

% Draw tracks
plot(x,y,'r.-','MarkerSize',20,'LineWidth',2);
% Draw heading and force
m = [x' y' [x+T.*cos(theta)]' [y+T.*sin(theta);]'];
plot([m(:,1) m(:,3)]',[m(:,2) m(:,4)]','b-','LineWidth',2);
grid on;

% Draw other stats
subplot(4,4,[3 4])
plot(dvx,'o-')
hold on;
plot(dvy,'o-')
grid on
grid minor
legend('ax','ay');

subplot(4,4,[7 8])
plot(T,'o-');
hold on;
plot(w,'o-');
plot(theta,'o-');
legend('T','w','theta');
grid on
grid minor

subplot(4,4,[11 12])
plot(dx,'o-');
hold on;
plot(dy,'o-');
legend('vx','vy');
grid on
grid minor
set(gcf, 'Position', [500, 500, 1200, 500])

subplot(4,4,[15 16])
spy(lms);
xlabel('Landmarks');
ylabel('Poses');
title('Observation Matrix');

end

