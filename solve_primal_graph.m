function [delta_x] = solve_primal_graph(x_k, last_delta_x, np, nl, nu, neq, J_obj, H_obj, F_eq, J_eq, H_eq, dt, mass)

ndim = size(x_k, 1);

% Primal Factor 1
% H = eye(ndim, ndim); % Debug
H = H_obj + H_eq;
f = J_obj;

% Primal Factor 2 (equality constraints)
Aeq = J_eq;
beq = F_eq;

A = [];
b = [];
lb = [];
ub = [];

options = optimoptions('quadprog','Algorithm','interior-point-convex','Display','iter-detailed', 'ConstraintTolerance', 1e-12);
last_delta_x
[delta_x, fval, exitflag] = quadprog(H, f, A, b, Aeq, beq, lb, ub, last_delta_x, options);
exitflag
fprintf('Primal graph err %f\n' ,fval);

% options = optimoptions('fmincon','Algorithm','active-set','Display','iter-detailed', 'ConstraintTolerance', 1e-12);
% last_delta_x
% [delta_x, fval, exitflag] = fmincon(@(x) 0.5*x'*H*x+f'*x, last_delta_x, A, b, Aeq, beq, lb, ub, [], options);
% exitflag
% fprintf('Primal graph err %f\n' ,fval);

end