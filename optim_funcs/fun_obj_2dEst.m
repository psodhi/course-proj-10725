function [fval] = fun_obj_2dEst(x, np, nl, nu, z, pair_idxs)

% cost function for a 2D estimation example

% x : (np*np_dim + nl*nl_dim + nu*nu_dim) x 1
% xp : np x 5 [x y psi vx vy]
% xl : nl x 2 [lx ly]
% xu : nu x 2 [T w]

xp = reshape(x(1:np*5), 5, np)';
xl = reshape(x(np*5+1:np*5+nl*2), 2, nl)';
xu = reshape(x(np*5+nl*2+1:np*5+nl*2+nu*2), 2, nu)';

% compute predicted (range, bearing) measurements based on current state estimate
% rel_coords = [(xl(pair_idxs(:,2), 1)-xp(pair_idxs(:,1), 1)) (xl(pair_idxs(:,2), 2)-xp(pair_idxs(:,1), 2))]; % nz x 2
% for i=1:size(rel_coords,1)
%     rel_coords(i,:) = (exp_map2(hat2(xp(pair_idxs(i,1), 3)))'*rel_coords(i,:)')';
% end
% range_pred = vecnorm(rel_coords')'; % nz x 1
% bearing_pred = atan2(rel_coords(:,2), rel_coords(:,1)); % nz x 1
% compute sum of errors between predicted and actual measurements
% err_meas = z-[range_pred bearing_pred]; % nz x 2
% err_meas = vecnorm(err_meas')'; % nz x 1
% fval = sum(err_meas);

num_obs = size(pair_idxs, 1); 
pose_idxs = pair_idxs(:,1);
lm_idxs = pair_idxs(:,2);

z_xl = zeros(num_obs, 2); % predicted landmark location
z_xl_pred = zeros(num_obs, 2);

z_xl_pred(:,1) = xp(pose_idxs,1) + z(:,1).*cos(xp(pose_idxs,3)+z(:,2));
z_xl_pred(:,2) = xp(pose_idxs,2) + z(:,1).*sin(xp(pose_idxs,3)+z(:,2));
z_xl(:,1) = xl(lm_idxs, 1);
z_xl(:,2) = xl(lm_idxs, 2);

% for k=1:num_obs
%     z_xl_pred(k,1) = xp(pose_idxs(k),1)+z(k,1)*cos(xp(pose_idxs(k),3)+z(k,2));    
%     z_xl_pred(k,2) = xp(pose_idxs(k),2)+z(k,1)*sin(xp(pose_idxs(k),3)+z(k,2));
% 
%     z_xl(k,1) = xl(lm_idxs(k), 1);
%     z_xl(k,2) = xl(lm_idxs(k), 2);
%     
%     % earlier cost function computing range and bearing errors
%     %lm_local = exp_map2(hat2(xp(pose_idx(k),3)))'*(xl(lm_idx(k),:) - xp(pose_idx(k),1:2))';
%     %z_pred(k,1) = sqrt(lm_local(1)^2 + lm_local(2)^2);
%     %z_pred(k,2) = atan2(lm_local(2), lm_local(1));
% end

init_point = [0 50 0 0 0];
err_meas = z_xl - z_xl_pred; % num_obs x 2

% err_meas(:,2) = wrapToPi(err_meas(:,2));

err_meas_norm = zeros(size(err_meas,1),1);
for i = 1:size(err_meas,1)
   % err_meas_norm(i) = norm(err_meas(i,:),2);%
   err_meas_norm(i) = err_meas(i,:)*err_meas(i,:)';
end
% err_meas = vecnorm(err_meas')'; % nz x 1

%err_prior = norm(init_point-xp(1,:));
err_prior = (init_point-xp(1,:))*(init_point-xp(1,:))';

fval = sum(err_meas_norm) + 10*err_prior;

end