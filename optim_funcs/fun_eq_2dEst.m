function [fval] = fun_eq_2dEst(x, np, nl, nu, neq, i, p, dt, mass)

% x : (np*np_dim + nl*nl_dim + nu*nu_dim) x 1
% xp_k : np x 5 [x y psi vx vy]
% xl_k : nl x 2 [lx ly]
% xu_k : nu x 2 [T w]

xp = reshape(x(1:np*5), 5, np)';
xl = reshape(x(np*5+1:np*5+nl*2), 2, nl)';
xu = reshape(x(np*5+nl*2+1:np*5+nl*2+nu*2), 2, nu)';
   
[dx1, dy1, dr1, dvx1, dvy1] = F(xp(i+1, 1), xp(i+1, 2), xp(i+1, 3), xp(i+1, 4), xp(i+1, 5), xu(i,1), xu(i,2), mass); % F(x(i+1),u(i))
[dx, dy, dr, dvx, dvy] = F(xp(i, 1), xp(i, 2), xp(i, 3), xp(i, 4), xp(i, 5), xu(i,1), xu(i,2), mass); % F(x(i),u(i))

if (p==1)
    fval = xp(i+1, 1) - xp(i, 1) - dt/2*(dx1+dx);
elseif (p==2)
    fval = xp(i+1, 2) - xp(i, 2) - dt/2*(dy1+dy);
elseif (p==3)
    fval = xp(i+1, 3) - vee2(log_map2(exp_map2(hat2(xp(i, 3)))*exp_map2(hat2(dt/2*(dr1+dr)))));
elseif (p==4)
    fval = xp(i+1, 4) - xp(i, 4) - dt/2*(dvx1+dvx);
elseif (p==5)
    fval = xp(i+1, 5) - xp(i, 5) - dt/2*(dvy1+dvy);
end

end