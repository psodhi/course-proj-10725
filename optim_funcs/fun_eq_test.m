function [fval] = fun_eq_test(x, j)

% j controls what equality constraint to return

if (j == 1)
    fval = x(1) - x(2) - 2;
elseif (j == 2)
    fval = x(1) - 2*x(3) - 4;
else
    fval = 0;
end

end