function [fval] = fun_obj_test(x)

fval = 0.5*x(1)^2 + x(2)^2 - x(1)*x(3) - 2*x(3) - 6*x(2);

end