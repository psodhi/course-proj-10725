function [x_kplus1, lambda_kplus1, delta_x] = optimize_sqp(x_k, last_delta_x, np, nl, nu, lambda_k, z, pair_idxs, dt, mass)

% for each kth iteration of the SQP,

% x_k : (np*np_dim + nl*nl_dim + nu*nu_dim) x 1
% xp_k : np x 5 [x y psi vx vy]
% xl_k : nl x 2 [lx ly]
% xu_k : nu x 2 [T w]
% xp_k = reshape(x(1:np*5), 5, np)';
% xl_k = reshape(x(np*5+1:np*5+nl*2), 2, nl)';
% xu_k = reshape(x(np*5+nl*2+1:np*5+nl*2+nu*2), 2, nu)';

n_dim = size(x_k,1); % dim of entire state vec

%% Hessian, Jacobian of Objective Function L(x)

J_obj = (jacobianest(@(x) fun_obj_2dEst(x, np, nl, nu, z, pair_idxs),  x_k))'; % n_dim x 1
H_obj = hessian(@(x) fun_obj_2dEst(x, np, nl, nu, z, pair_idxs), x_k); % n_dim x n_dim

%% Hessian, Jacobian of set of Equality Constraints F_j(x)

neq = np-1;
H_eq = zeros(n_dim, n_dim); % hessian
J_eq = zeros(neq*5, n_dim); % jacobian
F_eq = zeros(neq*5, 1); % equality function values
for j = 1:neq
    F_eq(5*(j-1)+1) = -fun_eq_2dEst(x_k, np, nl, nu, neq, j, 1, dt, mass);
    F_eq(5*(j-1)+2) = -fun_eq_2dEst(x_k, np, nl, nu, neq, j, 2, dt, mass);
    F_eq(5*(j-1)+3) = -fun_eq_2dEst(x_k, np, nl, nu, neq, j, 3, dt, mass);
    F_eq(5*(j-1)+4) = -fun_eq_2dEst(x_k, np, nl, nu, neq, j, 4, dt, mass);
    F_eq(5*(j-1)+5) = -fun_eq_2dEst(x_k, np, nl, nu, neq, j, 5, dt, mass);
    
    J_eq(5*(j-1)+1, :) = jacobianest(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 1, dt, mass), x_k);
    J_eq(5*(j-1)+2, :) = jacobianest(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 2, dt, mass), x_k);
    J_eq(5*(j-1)+3, :) = jacobianest(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 3, dt, mass), x_k);
    J_eq(5*(j-1)+4, :) = jacobianest(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 4, dt, mass), x_k);
    J_eq(5*(j-1)+5, :) = jacobianest(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 5, dt, mass), x_k);
    
    H_eq = H_eq + lambda_k(5*(j-1)+1)*hessian(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 1, dt, mass), x_k);
    H_eq = H_eq + lambda_k(5*(j-1)+2)*hessian(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 2, dt, mass), x_k);
    H_eq = H_eq + lambda_k(5*(j-1)+3)*hessian(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 3, dt, mass), x_k);
    H_eq = H_eq + lambda_k(5*(j-1)+4)*hessian(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 4, dt, mass), x_k);
    H_eq = H_eq + lambda_k(5*(j-1)+5)*hessian(@(x) fun_eq_2dEst(x, np, nl, nu, neq, j, 5, dt, mass), x_k);
end


%% Solve Primal Graph

fprintf('Solving Primal Graph ...\n');
delta_x = solve_primal_graph(x_k, last_delta_x, np, nl, nu, neq, J_obj, H_obj, F_eq, J_eq, H_eq, dt, mass);
x_kplus1 = x_k + delta_x;

% % overwrite angle update
% for i = 1:np
%     x_kplus1(5*(i-1)+3) = vee2(log_map2(exp_map2(hat2(x_k(5*(i-1)+3)))*exp_map2(hat2(delta_x(5*(i-1)+3)))));
% end

%% Solve Dual Graph

fprintf('Solving Dual Graph ...\n');
lambda_kplus1 = solve_dual_graph(x_k, np, nl, nu, neq, delta_x, J_obj, H_obj, J_eq, H_eq, dt, mass);

end